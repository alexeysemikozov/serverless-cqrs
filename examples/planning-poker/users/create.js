'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.create = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  if (typeof data.name !== 'string') {
    console.error('Validation Failed'); // eslint-disable-line no-console
    callback(new Error('Validation: Couldn\'t create a User. Please provide a new User name.'));
    return;
  }

  const params = {
    TableName: 'Events',
    Item: {
      id: uuid.v1(),
      name: 'userCreated',
      aggregate: {
        id: uuid.v1(),
        name: 'user'
      },
      payload: {
        name: data.name,
        enabled: data.enabled || false
      },
      timestamp: timestamp
    }
  };

  // write the user to the database
  dynamoDb.put(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error); // eslint-disable-line no-console
      callback(new Error('dynamoDb put: Couldn\'t create a User.'));
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
    callback(null, response);
  });
};
