'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.changeName = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  // validation
  if (typeof data.name !== 'string') {
    console.error('Validation Failed'); // eslint-disable-line no-console
    callback(new Error('Couldn\'t change User name. Please provide a User name'));
    return;
  }

const params = {
    TableName: 'Events',
    Item: {
      id: uuid.v1(),
      name: 'userChangedName',
      aggregate: {
        id: event.pathParameters.id,
        name: 'user'
      },
      payload: {
        name: data.name
      },
      timestamp: timestamp
    },
  };

  dynamoDb.put(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error); // eslint-disable-line no-console
      callback(new Error('Couldn\'t change name for User.'));
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
    callback(null, response);
  });
};
