'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.create = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  if (typeof data.title !== 'string') {
    console.error('Validation Failed'); // eslint-disable-line no-console
    callback(new Error('Validation: Couldn\'t create a Story. Please provide a story title'));
    return;
  }

  const params = {
    TableName: 'Events',
    Item: {
      id: uuid.v1(),
      name: 'storyCreated',
      aggregate: {
        id: uuid.v1(),
        name: 'story'
      },
      payload: {
        title: data.title
      },
      timestamp: timestamp
    },
  };

  // write the story to the database
  dynamoDb.put(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error); // eslint-disable-line no-console
      callback(new Error('dynamoDb put: Couldn\'t create a Story.'));
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
    callback(null, response);
  });
};
