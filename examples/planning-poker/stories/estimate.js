'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.estimate = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  // validation
  if (typeof data.userId !== 'string' || typeof data.estimation !== 'string') {
    console.error('Validation Failed'); // eslint-disable-line no-console
    callback(new Error('Couldn\'t estimate the Story.'));
    return;
  }

  const params = {
    TableName: 'Events',
    Item: {
      id: uuid.v1(),
      name: 'storyEstimated',
      aggregate: {
        id: event.pathParameters.id,
        name: 'story'
      },
      payload: {
        userId: data.userId,
        estimation: data.estimation
      },
      timestamp: timestamp
    },
  };

  dynamoDb.put(params, (error, result) => {
    if (error) {
      console.error(error); // eslint-disable-line no-console
      callback(new Error('Couldn\'t estimate the Story.'));
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
    callback(null, response);
  });
};
